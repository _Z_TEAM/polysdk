# PolySDK



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/_Z_TEAM/polysdk.git
git branch -M main
git push -uf origin main
```
add PATH User Environment

- %DevPSX%\PolySDK\tools
- %DevPSX%\tools\PCSXR
- %DevPSX%\cdrtfe-1.5.9portable\tools\cygwin
- %DevPSX%\cdrtfe-1.5.9portable\tools\cdrtools

## Installation

- [ ] [PSX.Dev Visual Studio Code Extension for PS1 Homebrew development.](https://www.youtube.com/watch?v=KbAv-Ao7lzU)
- [ ] [install msys2 and package mingw-w64-ucrt-x86_64-binutils-2.41-2 to build tools](https://www.msys2.org/)
- [ ] [install cdrtfe-1.5.9portable to get mkisofs](https://sourceforge.net/projects/cdrtfe/postdownload)
- [ ] [install pcsxr to run iso](https://ps1emulator.com/download)
- [ ] [install mvram](??)

## Liens

- [ ] [“Officiel” SDK compilation and samples.](https://github.com/ABelliqueux/nolibgs_hello_worlds#installation)
- [ ] [Orion tutorial installation.](http://onorisoft.free.fr/retro.htm?psx/tutorial/tuto.htm)

## 
 - v0 test main.c on pcsxr

## Collaborate with your team
# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thanks to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
